/* Chapter 1: Breaking the Surface: dive in: A Quick Dip */
// Exercise: Pool Puzzle - p24

/**
 * @author José Albano
 */
class Ex04PoolPuzzleOne {

  public static void main(String[] args) {
    int x = 0;

    while( x < 4 ) {

      System.out.print("a"); //---

      if ( x < 1 ) {
        System.out.print(" "); //---
      }

      System.out.print("n"); //---

      if( x > 1 ) {
        x = x + 1; //---
        System.out.print(" oyster "); //---
      }

      if ( x == 1 ) {
        System.out.print("noys "); //---
      }

      if ( x < 1 ) {
        System.out.print("oise "); //---
      }

      System.out.println();

      x = x + 1; //---

    } // while
  }
}