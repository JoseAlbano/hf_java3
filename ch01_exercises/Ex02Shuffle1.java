/* Chapter 1: Breaking the Surface: dive in: A Quick Dip */
// Exercise: Code Magnets p20

/**
 * @author José Albano
 */
class Ex02Shuffle1 {

  public static void main(String[] args) {

    int x = 3;

    while (x > 0) {

      if (x > 2) {
        System.out.print("a");
      }

      if (x == 2) {
        System.out.print("b c");
      }

      x = x - 1;
      System.out.print("-");
      
      if (x == 1) {

        System.out.print("d");

        x = x - 1;
      }

    } // while
  } // main
} // class