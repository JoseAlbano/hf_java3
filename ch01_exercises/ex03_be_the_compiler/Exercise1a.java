package ex03_be_the_compiler;

/* Chapter 1: Breaking the Surface: dive in: A Quick Dip */
// Exercise: BE the Compiler - Exercise A - p21

// My ANSWER: Compile, but Create an Infinite Loop.

/**
 * @author José Albano
 */
class Exercise1a {
  public static void main(String[] args) {
    
    int x = 1;
    
    while (x < 10) {
      if (x > 3) {
        System.out.println("big x");
      }

      // My FIX:
      x = x + 1;
    }
  }
}