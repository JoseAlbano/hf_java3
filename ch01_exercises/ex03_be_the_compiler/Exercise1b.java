package ex03_be_the_compiler;

/* Chapter 1: Breaking the Surface: dive in: A Quick Dip */
// Exercise: BE the Compiler - Exercise B - p21

// My ANSWER: 
// Doesn't Compile because is just a "main" method outside of a Class.

/**
 * @author José Albano
 */

// My FIX: Add the Class declaration
class Exercise1b {
  public static void main(String [] args) {
  
    int x = 5;
    
    while ( x > 1 ) {
      x = x - 1;
      
      if ( x < 3) {
        System.out.println("small x");
      }
    }
  }
}