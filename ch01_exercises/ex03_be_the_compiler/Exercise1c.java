package ex03_be_the_compiler;

/* Chapter 1: Breaking the Surface: dive in: A Quick Dip */
// Exercise: BE the Compiler - Exercise C - p21

// My ANSWER: Compile
// Doesn't Compile because the code doesn't inside a method.

/**
 * @author José Albano
 */
class Exercise1c {
  
  // My FIX: Put the code inside the "main" method.
  public static void main(String [] args) {
  
    int x = 5;
    
    while ( x > 1 ) {
      x = x - 1;
      
      if ( x < 3) {
        System.out.println("small x");
      }
    }
  } // main
}