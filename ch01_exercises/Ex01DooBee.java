/* Chapter 1: Breaking the Surface: dive in: A Quick Dip */
// Exercise: Sharpen your Pencil p15

class Ex01DooBee {
  public static void main(String[] args) {
    int x = 1;

    while (x < 3 ) {
      System.out.print("Doo");
      System.out.print("Bee");
      x = x + 1;
    }

    if (x == 3 ) {
      System.out.println("Do");
    }
  }
}
