package ex01_mixed_messages;
/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// Exercise: Mixed Messages p194

class C extends B {
  void m3() {
    System.out.print("C's m3, " + (ivar + 6));
  }
}