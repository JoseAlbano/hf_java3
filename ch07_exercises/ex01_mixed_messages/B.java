package ex01_mixed_messages;
/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// Exercise: Mixed Messages p194

class B extends A {
  void m1() {
    System.out.print("B's m1, ");
  }
}