package ex03_pool_puzzle;

/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// puzzle: Pool Puzzle p196

public class TestBoats {
  public static void main(String[] args) {
    Boat b1 = new Boat();             // _________ b1 = new Boat();
    Sailboat b2 = new Sailboat();     // Sailboat b2 = new __________();
    Rowboat b3 = new Rowboat(); // Rowboat ________ = new Rowboat();

    b2.setLength(32);
    b1.move();  // b1.__________();
    b3.move();  // b3.__________();
    b2.move();   // _______.move();
  }
}