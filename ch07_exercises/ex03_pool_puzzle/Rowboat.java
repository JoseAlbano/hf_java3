package ex03_pool_puzzle;

/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// puzzle: Pool Puzzle p196

public class Rowboat extends Boat {     // public class Rowboat ________ ________ {
  public void rowTheBoat() {            //  public ___________ rowTheBoat() {
    System.out.print("stroke natasha");
  }
}