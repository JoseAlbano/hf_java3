package ex03_pool_puzzle;

/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// puzzle: Pool Puzzle p196

public class Sailboat extends Boat {  // public class __________ ________ Boat {
  public void move() {                //  public _______ _________() {
    System.out.print("hoist sail ");   //    System.out.print("___________");
  }
}