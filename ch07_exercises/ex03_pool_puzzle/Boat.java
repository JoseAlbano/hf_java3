package ex03_pool_puzzle;

/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// puzzle: Pool Puzzle p196

public class Boat {
  private int length ; //private int __________ ;
  
  public void setLength ( int len ) { //_______ void _________ ( ______ ) {
    length = len;
  }

  public int getLength() {
    return length;  // ________ _________ ;
  }

  public void move() { // public ___________ move() {
    System.out.print("drift ");  // System.out.print("___________");
  }
}