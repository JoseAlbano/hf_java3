package ex02_be_the_compiler;
/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// Exercise: Be the Compiler p195

class Monster {

  boolean frighten(int d) {
    System.out.println("arrrgh");
    return true;
  }
}