package ex02_be_the_compiler;
/* Chapter 7: Better Living in Objectville: Inheritance and Polymorphism */
// Exercise: Be the Compiler p195

class Vampire extends Monster {
  boolean frighten(int x) {
    System.out.println("a bite?");
    return false;
  }
}