package ex03_pool_puzzle;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Pool Puzzle p44

public class EchoTestDrive {
  public static void main(String[] args) {
    Echo e1 = new Echo();
    Echo e2 = new Echo(); // ___________

    int x = 0;

    while ( x < 4 ) { // ___________
      e1.hello();
      e1.count = e1.count + 1; // __________________________
      
      if ( x == 3 ) { // ____________
        e2.count = e2.count + 1;
      }

      if ( x > 0 ) { // ____________
        e2.count = e2.count + e1.count;
      }

      x = x + 1;
    }

    System.out.println(e2.count);
  }
}