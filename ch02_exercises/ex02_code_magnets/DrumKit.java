package ex02_code_magnets;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Code Magnets p43

class DrumKit {
  boolean topHat = true;
  boolean snare = true;

  void playSnare() {
    System.out.println("bang bang ba-bang");
  }

  void playTopHat() {
    System.out.println("ding ding da-ding");
  }
}