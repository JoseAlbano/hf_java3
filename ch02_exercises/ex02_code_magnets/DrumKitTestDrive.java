package ex02_code_magnets;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Code Magnets p43

class DrumKitTestDrive {
  public static void main(String[] args) {
    DrumKit d = new DrumKit();

    d.playSnare();

    d.snare = false;

    if (d.snare == true) {
      d.playSnare();
    }

    d.playTopHat();
  }
}