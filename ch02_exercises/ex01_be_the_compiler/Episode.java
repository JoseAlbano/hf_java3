package ex01_be_the_compiler;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Be the Compiler p42

class Episode {
  int seriesNumber;
  int episodeNumber;

  void skipIntro() {
    System.out.println("Skipping intro...");
  }

  void skipToNext() {
    System.out.println("Loading next episode...");
  }

  // My Fix:
  void play() {
    System.out.println("Playing episode...");
  }
}