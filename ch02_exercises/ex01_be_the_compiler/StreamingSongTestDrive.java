package ex01_be_the_compiler;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Be the Compiler p42

class StreamingSongTestDrive {
  public static void main(String[] args) {

    // It doesn't compile. StreamingSong object not created.

    // My Fix:
    StreamingSong song = new StreamingSong();

    song.artist = "The Beatles";
    song.title = "Come Together";
    
    song.play();
    song.printDetails();
  }
}