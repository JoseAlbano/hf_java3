package ex01_be_the_compiler;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Be the Compiler p42

class EpisodeTestDrive {
  public static void main(String[] args) {

    // It doesn't compile. play method not declared in Episode class.
    
    Episode episode = new Episode();
    
    episode.seriesNumber = 4;
    
    episode.play();
    episode.skipIntro();
  }
}