package ex01_be_the_compiler;
/* Chapter 2: A Trip to Objectville: Classes and Objects */
// Exercise: Be the Compiler p42

class StreamingSong {
  String title;
  String artist;
  int duration;

  void play() {
    System.out.println("Playing song");
  }

  void printDetails() {
    System.out.println("This is " + title + " by " + artist);
  }
}