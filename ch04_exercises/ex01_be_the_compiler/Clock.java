package ex01_be_the_compiler;
/* Chapter 4: How Objects Behave: Methods use Instance Variables */
// Exercise: Be the Compiler p88

// It doesn't compile. The getTime has a void modifier and it isn't public.

class Clock {
  String time;
  
  void setTime(String t) {
    time = t;
  }

  // My Fix:
  //void getTime() {
  public String getTime() {
    return time;
  }
}