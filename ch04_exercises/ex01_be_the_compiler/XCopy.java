package ex01_be_the_compiler;
/* Chapter 4: How Objects Behave: Methods use Instance Variables */
// Exercise: Be the Compiler p88

// It compile. Output: 42 82

class XCopy {
  
  public static void main(String[] args) {
    
    int orig = 42;
    XCopy x = new XCopy();
    
    int y = x.go(orig);
    System.out.println(orig + " " + y);
  }

  int go(int arg) {
    arg = arg * 2;
    return arg;
  }
}