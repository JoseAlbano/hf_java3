package ex01_be_the_compiler;
/* Chapter 4: How Objects Behave: Methods use Instance Variables */
// Exercise: Be the Compiler p88

class ClockTestDrive {

  public static void main(String[] args) {
    Clock c = new Clock();
  
    c.setTime("1245");
    String tod = c.getTime();
    
    System.out.println("time: " + tod);
  }
}