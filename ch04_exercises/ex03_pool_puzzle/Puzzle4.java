package ex03_pool_puzzle;
/* Chapter 4: How Objects Behave: Methods use Instance Variables */
// Exercise: Pool Puzzle p91

public class Puzzle4 {
  
  public static void main(String [] args) {
    
    Value[] values = new Value[6]; // ___________________________________
    int number = 1;
  
    int i = 0;
    while (i < 6) {
      values [i] = new Value();     // ___________________________
      values[i].intValue = number;  // ___________________________
      
      number = number * 10;
      
      i = i + 1; // _________________
    }

    int result = 0;
    
    i = 6;
    while (i > 0) {
      i = i - 1;    // _________________
      result = result + values[i].doStuff(i); // ___________________
    }

    System.out.println("result " + result);
  }
}

class Value {   // ____________
  int intValue;
  
  public int doStuff(int factor) { // ________ ______ doStuff(int _________) {
    if (intValue > 100) {
      return intValue * factor; // _________________________
    } else {
      return intValue * (5 - factor); // _________________________
    }
  }
}