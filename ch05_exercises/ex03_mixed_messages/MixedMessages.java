package ex03_mixed_messages;
/* Chapter 05: Extra-Strength Methods: Writing a Program */
// Exercise: Mixed Messages p121

public class MixedMessages {
  public static void main(String[] args) {
    int x = 0;
    int y = 30;
    
    for (int outer = 0; outer < 3; outer++) {
      for (int inner = 4; inner > 1; inner--) {
      
        // Candidate code goes here
        // x = x + 3;// x = x + 6;// x = x + 2;// x++;//x--;
        x = x + 0;
        y = y - 2;
        
        if (x == 6) {
          System.out.println("\tBREAK inner " + inner + "\t " + x + " " + y);
          break;
        }
        
        x = x + 3;

        System.out.println("\tinner " + inner + "\t " + x + " " + y);
      }

      y = y - 2;
      System.out.println("outer " + outer + "\t\t " + x + " " + y);
    }

    System.out.println(x + " " + y);
  }
} // class