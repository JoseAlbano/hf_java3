package ex02_code_magnets;
/* Chapter 05: Extra-Strength Methods: Writing a Program */
// Exercise: Code Magnets p118

class MultiFor {

    public static void main(String[] args) {
      
      for(int i = 0; i < 4; i++) {
        
        for(int j = 4; j > 2; j--) {

          System.out.println(i + " " + j);
        } // for j

        if (i == 1) {
          i++;
        } // if

      } // for i
    } // main
} // class