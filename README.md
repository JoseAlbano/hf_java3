# My Learning Path: Java Software Developer

## Book: *Head First Java (3rd Edition)* by *Katy Sierra, Bert Bates and Trisha Gee*

Hello.

Here are my Solutions to the Exercises in the Book.

### My Progress

- [x] Chapter 01: Breaking the Surface: dive in: A Quick Dip
- [x] Chapter 02: A Trip to Objectville: Classes and Objects
- [x] Chapter 03: Know Your Variables: Primitives and References
- [x] Chapter 04: How Objects Behave: Methods use Instance Variables
- [x] Chapter 05: Extra-Strength Methods: Writing a Program
- [x] Chapter 06: Using the Java Library: Get to know the Java API
- [x] Chapter 07: Better Living in Objectville: Inheritance and Polymorphism
- [x] Chapter 08: Serious Polymorphism: Interfaces and Abstract Classes
- [ ] Chapter 09: Life and Death of an Object: Constructors and Garbage Collection
- [ ] Chapter 10: Numbers Matter: Numbers and Statics
- [ ] Chapter 11: Data Structures: Collections and Generics
- [ ] Chapter 12: What, Not How: Lambdas and Streams
- [ ] Chapter 13: Risky Behavior: Exception Handling
- [ ] Chapter 14: A Very Graphic Story: Intro to GUI, Event Handling, and Inner Classes
- [ ] Chapter 15: Work on Your Swing: Using Swing
- [ ] Chapter 16: Saving Objects (and Text): Serialization and File I/O
- [ ] Chapter 17: Make a Connection: Networking and Threads
- [ ] Chapter 18: Dealing with Concurrency Issues: Race Conditions and Immutable Data

