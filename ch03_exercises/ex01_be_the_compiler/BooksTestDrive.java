package ex01_be_the_compiler;
/* Chapter 3: Know Your Variables: Primitives and References */
// Exercise: Be the Compiler p63

class BooksTestDrive {
  public static void main(String[] args) {
    Books[] myBooks = new Books[3];
    int x = 0;

    // Doesn't Compile. Doesn't create the Books objects.
    // My Fix:
    myBooks[0] = new Books();
    myBooks[1] = new Books();
    myBooks[2] = new Books();

  
    myBooks[0].title = "The Grapes of Java";
    myBooks[1].title = "The Java Gatsby";
    myBooks[2].title = "The Java Cookbook";
    myBooks[0].author = "bob";
    myBooks[1].author = "sue";
    myBooks[2].author = "ian";
    
    while (x < 3) {
      System.out.print(myBooks[x].title);
      System.out.print(" by ");
      System.out.println(myBooks[x].author);
      x = x + 1;
    }
  }
}