package ex03_pool_puzzle;
/* Chapter 3: Know Your Variables: Primitives and References */
// Exercise: Pool Puzzle p65

class Triangle {

  double area;
  int height;
  int length;

  public static void main(String[] args) {

    int x = 0;  //____________
    Triangle[] ta = new Triangle[4];  //_______________________
    
    while ( x < 4 ) {
      ta[x] = new Triangle();     //__________________________
      ta[x].height = (x + 1) * 2; //________.height = (x + 1) * 2;
      ta[x].length = x + 4;       //________.length = x + 4;
      ta[x].setArea();            //__________________________
    
      System.out.print("triangle " + x + ", area");
      System.out.println(" = " + ta[x].area); 
      
      x = x + 1;  //________________
    }

    int y = x;  //______________
    
    x = 27;
    
    Triangle t5 = ta[2];
    ta[2].area = 343;
    
    System.out.print("y = " + y);
    System.out.println(", t5 area = " + t5.area);
  }

  void setArea() {
    area = (height * length) / 2; //____________ = (height * length) / 2;
  }
}