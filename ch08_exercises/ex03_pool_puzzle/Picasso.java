package ch08_exercises.ex03_pool_puzzle;
/* Chapter 8: Serious Polymorphism */
// Exercise 3: Pool Puzzle

/**
 * @author José Albano
 */
public class Picasso implements Nose {
  @Override
  public int iMethod() {
    return 7;
  }  
}
