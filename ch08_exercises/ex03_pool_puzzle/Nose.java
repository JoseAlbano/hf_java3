package ch08_exercises.ex03_pool_puzzle;
/* Chapter 8: Serious Polymorphism */
// Exercise 3: Pool Puzzle

/**
 * @author José Albano
 */
public interface Nose {
  public int iMethod() ;
}
